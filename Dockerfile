FROM golang:1.8

#env
RUN mkdir -p /app/gorest
WORKDIR /app/gorest

#copy and build golang app
COPY main.go /app/gorest
RUN go get github.com/gorilla/mux
RUN go build

#add vue app
ADD frontend /app/gorest/frontend

#bind port
EXPOSE 8080:8080